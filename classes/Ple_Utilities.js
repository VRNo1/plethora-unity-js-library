#pragma strict


class Ple_Utilities{



function Ple_Utilities(){

}

function reMap(val : float, from1 : float, to1 : float, from2 : float, to2 : float){
return (val - from1) / (to1 - from1) * (to2 - from2) + from2;
}

function GetCubicCoordinates(t : float, p0 : Vector3 , p1 :Vector3 , p2 : Vector3, p3 : Vector3 ) : Vector3{
	return Mathf.Pow(1-t,3)*p0 + 3 *Mathf.Pow(1-t,2)*t*p1 +  
	3*(1-t)* Mathf.Pow(t,2)*p2 + Mathf.Pow(t,3)*p3;
}

  function rotateAroundAxis(v : Vector3, axis : Vector3,  theta : float) {
 		var result : Vector3 = Vector3(0,0,0);
 		
 		var ax = axis.x;
 		var ay = axis.y;
 		var az = axis.z;
 		
 		var ux = ax * v.x;
 		var uy = ax * v.y;
 		var uz = ax * v.z;
 		
 		var vx = ay * v.x;
 		var vy = ay * v.y;
 		var vz = ay * v.z;
 		
 		var wx = az * v.x;
 		var wy = az * v.y;
 		var wz = az * v.z;
 		
 		var si  : float = Mathf.Sin(theta);
 		var co  : float = Mathf.Cos(theta);
 		
        var xx : float = (ax * (ux + vy + wz)
                + (v.x * (ay * ay + az * az) - ax * (vy + wz)) * co + (-wy + vz)
                * si);
        var yy : float =  (ay * (ux + vy + wz)
                + (v.y * (ax * ax + az * az) - ay * (ux + wz)) * co + (wx - uz)
                * si);
        var zz  : float = (az * (ux + vy + wz)
                + (v.z * (ax * ax + ay * ay) - az * (ux + vy)) * co + (-vx + uy)
                * si);
                        
        result.x = xx; 
        result.y = yy; 
        result.z = zz;        

        return result;
    }



}