#pragma strict

//NOT WORKING:
class Ple_Spline3D{

//list of points:
var points = new Array();;

function Ple_Spline3D(){
//points = new Array();
}

//functions:
//add points
function addPoints(v : Vector3){
points.Push(v);
}
//clear array:
function clear(){
points.clear();
}

//get num of points
function numPoints(){
return points.length;
}
//get point list:
//smooth spline:
//point on param:
function ptOnParam(param : float){
	var tlen = 0;
	var count = points.length - 1;
	var acuLengs : float [];
	acuLengs = new float [count];
	var lengs : float [];
	lengs = new float [count];
	
	for(var i = 1; i < points.length; i++){
		var v : Vector3 = points[i];
		var bef : Vector3 = points[i-1];
		var dif : Vector3 = v - bef;
		var d : float = dif.magnitude;
		tlen += d;
		lengs[i-1] = d;
		acuLengs[i-1] = tlen;
	}
	var linearPos : float = tlen * param;
	var segment : int = 0;
	var loclen : float = linearPos;
	
	//determine in what segment you are:
	for(var j : int = 0; j < count; j++){
		if(linearPos > acuLengs[j]){
			segment = j + 1;
		}
	}
	
	//determine your distance in your own segment:
	for (var k : int = 0; k < segment; k++){
		loclen -= lengs[k];
	}
	
	var v1 : Vector3 = points[segment+1];
	var bef1 : Vector3 = points[segment];
	
	var dif2 : Vector3 = v1 - bef1;
	dif2 = dif2.normalized;
	dif2 *= loclen;
	dif2 += bef1;


return dif2;
}


}