#pragma strict

class Ple_Pendulum{

private var origin : Vector3 = Vector3(0,0,0);
private var loc : Vector3 = Vector3(0,0,0);
private var r : float = 10.0;             // Length of arm
private var angle : float = Mathf.PI/9;         // Pendulum arm angle
private var aVelocity: float = 0.0;     // Angle velocity
private var aAcceleration: float = 0.0; // Angle acceleration
//private var ballr: float = 1.0;         // Ball radius
private var damping: float = 1.0;       // Arbitary damping amount
private var gravity : float = 0.01;  

//private var dragging : boolean = false;

function Ple_Pendulum( ori : Vector3, rad : float, ang : float){
origin = ori;
r = rad;
angle = ang;

}

function setRadius(v : float){
r = v;
}

function getLocation(){
return loc;
}

function getOrigin(){
return origin;
}

function setOrigin(v : Vector3){
origin = v;
}

function setDamping(v : float){
damping = v;
}

function setGravity(v : float){
gravity = v;
}

function calculate(){
	                            // Arbitrary constant
	aAcceleration = (1 * gravity / r) * Mathf.Sin(angle);  // Calculate acceleration (see: http://www.myphysicslab.com/pendulum1.html)
	aVelocity += aAcceleration;                 // Increment velocity
	aVelocity *= damping;                       // Arbitrary damping
	angle += aVelocity;                         // Increment angle
	
	loc = Vector3(r*Mathf.Sin(angle), r*Mathf.Cos(angle), 0);         // Polar to cartesian conversion
	loc += (origin); 
}

}


