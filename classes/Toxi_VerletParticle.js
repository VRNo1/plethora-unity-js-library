#pragma strict

class Toxi_VerletParticle {

var loc : Vector3;
var temp : Vector3;
var prev : Vector3; //velocity

var weight : float;
var invWeight : float;


var force : Vector3;
var isLocked : boolean;


function Toxi_VerletParticle(_x : float,_y : float,_z : float){

 loc = Vector3(_x,_y,_z);
 prev = Vector3(_x,_y,_z); //velocity

 weight = 1.0;
 invWeight = 1.0f / 1.0;
}


//functions:
//addForce
//addVelocity

function addForce (v : Vector3){
	force += v;
	return this;
}

function update(){

	if(!isLocked){
		//apply Behaviours
		applyForce();
		//apply constrains
	}

}

function applyForce(){
	
	temp.Set(loc.x,loc.y,loc.z);
	var dif : Vector3 = loc -  prev;
	dif += force;
	loc += dif;
	prev.Set(temp.x,temp.y,temp.z);
	force.Set(0,0,0);

}

//setters:
function setLoc(v : Vector3){
	loc = v;
}

function setLock(val : boolean){
	isLocked = val;
}

 function  setWeight( w : float) {
  weight = w;
  invWeight = 1f / w;
  }

function clearForce(){
	force.Set(0,0,0);
}

function scaleVelocity( scl : float) {
    prev = interpolateToSelf(prev, loc, 1.0f - scl);
        //return this;
}

function interpolateToSelf( main : Vector3, v : Vector3,  f : float) {
    main.x += (v.x -  main.x) * f;
    main.y += (v.y -  main.y) * f;
    main.z += (v.z -  main.z) * f;
    return main;
}

function clearVelocity(){
	prev.Set(loc.x,loc.y,loc.z);
}

//getters:
function getPreviousLocation(){
	return prev;
}
function getVelocity(){
	var dif : Vector3 = loc -  prev;
	return dif;
}


}



