#pragma strict
import System.Collections.Generic;

class Toxi_VerletPhyisics3D{

//dinamic particle list
var particleList : List.<Toxi_VerletParticle> = new List.<Toxi_VerletParticle>();
//dynamic spring list
var springsList : List.<Toxi_VerletSpring> = new List.<Toxi_VerletSpring>();
//iterations
var numIterations : int;
//gravity
var grav : Vector3;
//drag
var drag : float;

/*
var myList : List.<Type> = new List.<Type>();  // declaration
var someNumbers = new List.<int>();            // a real-world example of declaring a List of 'ints'
var enemies = new List.<GameObject>();         // a real-world example of declaring a List of 'GameObjects'
myList.Add(theItem);                           // add an item to the end of the List
myList[i] = newItem;                           // change the value in the List at position i
var thisItem = List[i];                        // retrieve the item at position i
myList.RemoveAt(i);                            // remove the item from position i
*/

function Toxi_VerletPhyisics3D(){

//particles = new Array();
//springs = new Array();

numIterations = 1;
grav = Vector3(0,-1,0);

drag = 0.01;

}
//--------------------------------------------------------------------------------
//APPLY TO ALL:
function applyForce(v : Vector3){
	for (var pa : Toxi_VerletParticle in particleList){
		pa.addForce(v);
	}
}
function setSpringStrength(d : float){
	for (var s : Toxi_VerletSpring in springsList){
		s.setStrength(d);
	}
}
function setSpringRestLength(d : float){
	for (var s : Toxi_VerletSpring in springsList){
		s.setRestLength(d);
	}
}
//--------------------------------------------------------------------------------
//SETTERS:
function setDrag(v : float){
	drag = v;
}
//--------------------------------------------------------------------------------
//GETTERS:
function getParticle(i : int){
	if(i < particleList.Count){
	return particleList[i];
	}
}

//--------------------------------------------------------------------------------

//ADD ELEMENTS:
function addParticle(p : Toxi_VerletParticle){
	particleList.Add(p);
}

function addSpring(s : Toxi_VerletSpring){
	//add a check if that spring is in the system already
	springsList.Add(s);
}

function clearAll(){
	particleList.Clear();
	springsList.Clear();
}
//--------------------------------------------------------------------------------
//UPDATES:
function update(){
	updateParticles();
	updateSprings();
	return (this);
}

function updateRigid(){
	updateParticles();
	updateRigidSprings();
	return (this);
}

function updateParticles(){
	for (var p : Toxi_VerletParticle in particleList) {
	    p.scaleVelocity(drag);
	    p.update();    
	}
}

function updateSprings(){
	if(springsList.Count > 0){
		for(var i = numIterations; i > 0; i--){
			for (var s : Toxi_VerletSpring in springsList) {
			    s.update();    
			}
		}
	}
}

function updateRigidSprings(){
	if(springsList.Count > 0){
		for(var i = numIterations; i > 0; i--){
			for (var s : Toxi_VerletSpring in springsList) {
			    s.updateRigid();    
			}
		}
	}
}
//--------------------------------------------------------------------------------

































}