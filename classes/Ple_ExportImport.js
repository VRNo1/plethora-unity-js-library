#pragma strict
import System;
import System.IO;

////ref: http://forum.unity3d.com/threads/8864-How-to-write-a-file

class Ple_ExportImport{

function Ple_ExportImport(){

}

//FUNCTIONS TO WRITE:
// 1 - EXPORT POSITIONS AND ROTATIONS OF GAMEOBJECTS
// 2 - EXPORT MESH 
// 3 - Export CSV files for exel sheets
// 4 - Export PDF?????


//EXAMPLE HOW TO WRITE A FILE: >>>> can be txt, csv, obj, etc...!!!
function writeTextFile(fileName : String){

        if (File.Exists(fileName)) {
            Debug.Log(fileName + " already exists.");
            return;
        }
        
        var sr = File.CreateText(fileName);
        sr.WriteLine ("This is my file.");
        sr.WriteLine ("I can write ints {0} or floats {1}, and so on.",  1, 4.2);
        sr.Close();
}

//EXAMPLE HOW TO READ A FILE:
function ReadFile(fileName : String){
    if(File.Exists(fileName)){
        var sr = File.OpenText(fileName);
        var line = sr.ReadLine();
        while(line != null){
            Debug.Log(line); // prints each line of the file
            line = sr.ReadLine();
        }   

    } else {
        Debug.Log("Could not Open the file: " + fileName + " for reading.");
        return;
    }
}

}




 



 
