#pragma strict

class Ple_MeshPipe{

private var pUtil : Ple_Utilities = new Ple_Utilities();

//segments on section
var segments : int = 6;
//total radius
var rad : float = 0.5;

private var numPts: int = 6;

//MESH:
//private var ml : Mesh;
private var lmat : Material;
private var tran : Transform;

//personal attempt:
private var jMesh : Mesh;
private var vertxs : Vector3 [];
private var trings : int [];
private var uvs : Vector2[];
private var norms : Vector3[];
private var tangs : Vector3[];

var debugOn : boolean = false;


function Ple_MeshPipe(_tran : Transform){
	tran = _tran;
	jMesh = new Mesh();
}

function setDebug(val : boolean){
	debugOn = val;
}

function setSegments(val : int){
	segments = val;
}

function setRadius(val : float){
	rad = val;
}

function createVertices(points : List.<Vector3>){

	//vertices.Clear();
	numPts = points.Count;
	
	//var vCount = 0;
	var vCount2 = 0;
	var tCount = 0;
	
	///*
	//------------------------------------------------------------
	//JMESH VARIABLES:
	jMesh.Clear();
	vertxs = new Vector3[numPts * segments];
	trings = new int [     (((numPts-1) * (segments-1)   )) * 6];
	uvs = new Vector2[numPts * segments];
	norms = new Vector3[numPts * segments];
	tangs = new Vector3[numPts * segments];
	//------------------------------------------------------------
	//*/
	
	for (var j : int = 0 ; j < points.Count-1 ; j++ ){
	
		var n = Vector3.Cross(points[j], points[j+1]);
		n.Normalize();
		var dir : Vector3 = points[j+1]- points[j];
		dir.Normalize();
		var wid : Vector3 = Vector3.Cross(n,dir);
		wid.Normalize();
		
		var widPlus : Vector3 = wid + points[j];
		if (debugOn)Debug.DrawLine(points[j],widPlus,  Color.yellow);
		var dirPlus : Vector3 = dir + points[j];
		if (debugOn)Debug.DrawLine(points[j],dirPlus,  Color.cyan);
		
		for (var i : int = 0 ; i < segments ; i++ ){
			var widCopy : Vector3 = Vector3(wid.x,wid.y,wid.z);
			widCopy.Normalize();
			var angle : float = (Mathf.PI * 2 / (segments-1)) * i;
			var pt : Vector3 = pUtil.rotateAroundAxis(widCopy, dir, angle);
			pt.Normalize();
			var nooMal : Vector3 = Vector3(pt.x,pt.y,pt.z); // make a copy of the normal:
			pt *= rad;
			pt += points[j];

			//MAKE JMESH:
			//1 add the vertex (v3)
			vertxs[vCount2] = pt;//tran.InverseTransformPoint(pt); //pt;
			//2 add 3 indices of connection (3 ints)
			if((vCount2+1) % segments != 0 && vCount2 < (segments * (numPts -1))  ){
			//triangle 1:
			trings[tCount] = vCount2;				    tCount++;
			trings[tCount] = vCount2+1;				    tCount++;
			trings[tCount] = vCount2 + segments+1;		tCount++;
			//triangle 2:
			trings[tCount] = vCount2;					tCount++;
			trings[tCount] = vCount2 + segments+1;		tCount++;
			trings[tCount] = vCount2 + segments;		tCount++;
			}
			//3 add the uv data (v2)
			uvs[vCount2] = Vector2(0,0);
			//4 add the normal data (v3)
			norms[vCount2] = nooMal;
			//4 add the tangent data (v3)
			//tangs[vCount2] = pt;
			vCount2++;
			//------------------------------------------------------------
			//*/
			
		}
		//------------------------------------------------------------------------------------
		//last point:-------------------------------------------------------------------------//NOT WORKING YET...
		if(j == points.Count-2){
			var n2 = Vector3.Cross(points[points.Count-2], points[points.Count-1]);
			n2.Normalize();
			var dir2 : Vector3 = points[points.Count-1]- points[points.Count-2];
			dir2.Normalize();
			var wid2 : Vector3 = Vector3.Cross(n2,dir2);
			wid2.Normalize();
		
			var widPlus2 : Vector3 = wid2 + points[points.Count-1];
			if (debugOn)Debug.DrawLine(points[points.Count-1],widPlus2,  Color.yellow);
			
			var dirPlus2 : Vector3 = dir2 + points[points.Count-1];
			if (debugOn)Debug.DrawLine(points[points.Count-1],dirPlus2,  Color.cyan);
			
			for (var q : int = 0 ; q < segments ; q++ ){
				var widCopy2 : Vector3 = Vector3(wid2.x,wid2.y,wid2.z);
				widCopy2.Normalize();
				var angle2 : float = (Mathf.PI * 2 / (segments-1)) * q;
				var pt2 : Vector3 = pUtil.rotateAroundAxis(widCopy2, dir2, angle2);
				pt2.Normalize();
				var nooMal2 : Vector3 = Vector3(pt2.x,pt2.y,pt2.z); // make a copy of the normal:
				pt2 *= rad;
				pt2 += points[j+1];
				//vertices.Add(pt2);	
				
				//add final vertices:
				vertxs[vCount2] = pt2;//tran.InverseTransformPoint(pt2);
				uvs[vCount2] = Vector2(0,0);
				norms[vCount2] = nooMal2;
				vCount2++;
			}
		}
		//------------------------------------------------------------------------------------
			
		
	}
	
	//draw all vertices in debug mode:
	for (var k : int = 1 ; k < vertxs.length ; k++ ){	
		if (debugOn)Debug.DrawLine(vertxs[k], vertxs[k-1],  Color.green);
	}

	//------------------------------------------------------------
	//MAKE JMESH // OUTSIDE THE LOOPS!
	jMesh.vertices = vertxs;
	jMesh.triangles = trings;
	jMesh.RecalculateBounds();
	jMesh.uv = uvs;
	jMesh.normals = norms;
	//jMesh.tangents = tangs; //tangents need to be Vector4!!! :S			
	//------------------------------------------------------------	

}

function setMaterial(mat : Material){
	lmat = mat;
}

function clearMesh(){
	jMesh.Clear();
}

function drawMesh() {
	//Graphics.DrawMesh(jMesh, tran.localToWorldMatrix, lmat, 0);
	Graphics.DrawMesh(jMesh, Vector3.zero, Quaternion.identity, lmat, 0);
	//Graphics.DrawMesh(jMesh, tran.worldToLocalMatrix, lmat, 0);
}




}