#pragma strict

class Ple_Particle{

var loc : Vector3;
var vel : Vector3;
var acc : Vector3;

var futLoc : Vector3;

var radius : float;
var mass : float = 24.0;

var speedFrictionThreshold : float = 0.2f; //20
var maxVel : float = 0.4f;

var friction : float = 0.001f;
//var stiff : float = 0.5f;

var anchor : boolean = false;

var damp : float = 0.98;

//main 3 values from spring: g = grav, f = friction, k = stiffness
//thresholds = f(0 -> 1, def 0.1)       k(0 -> 0.75, def 0.5)         grav(0 -> 4, def 0)

function Ple_Particle(_loc : Vector3){

this.loc = _loc;
this.vel = Vector3(0,0,0);
this.acc = Vector3(0,0,0);

this.futLoc = Vector3(0,0,0);
}

//update the location based on forces
function update(){
	
	if(!anchor){
		//quicker square magnitude:
		//var speed = vel.sqrMagnitude;
		var speed = vel.magnitude;
		
		
		
		var fs : float = 1.0-friction;
		
		if(speed > speedFrictionThreshold){
		
		fs *= speedFrictionThreshold/speed;
	    vel *= fs;

	 	vel = Vector3.ClampMagnitude(vel, maxVel);
	 	
	    loc += vel;	
		}
	}

}

function update2(){
	
	if(!anchor){
		
		vel += acc;
		vel *= damp; 

	 	//vel = Vector3.ClampMagnitude(vel, maxVel);
	 	
	    loc += vel;	
	    
	    acc = Vector3(0,0,0);
		}
}
	
function actualize(){
	//loc = futLoc;
}	


function setFriction(v : float){
	friction = v;
}

function setMass(v : float){
	mass = v;
}

function setDamp(v : float){
	damp = v;
}

function addForce(v : Vector3){
	vel += v;
}
function addForce2(v : Vector3){
	v /= mass;
	acc += v;
}

//stay inside boundry area
function boundry(){

}

function setAnchor(v : boolean){
	anchor = v;
}





}