#pragma strict
//-------------------------------------------------------------------
//UNITY CUSTOM CLASSES
//BY JOSE SANCHEZ
//DEVELOPED FOR: PLETHORA-PROJECT.COM
//-------------------------------------------------------------------

//WORKING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//MESH CREATION FOR LINES
//QUICKER AND BETTER THAN LINE RENDERER:
//-------------------------------------------------------------------


class Ple_LineMesh{

private var ml : Mesh;
private var lmat : Material;
private var w : float = 1;
private var tran : Transform;

private var points = new Array();

private var myObject : GameObject;

private var lookCam : boolean = false;
private var vecDirection : Vector3;

function Ple_LineMesh( _myObject: GameObject){
	ml = new Mesh();
	tran = _myObject.transform;
	myObject = _myObject;
	
	vecDirection = myObject.transform.forward;
}

function clearMesh(){
	ml.Clear();
}

function setCamLook(val : boolean){
	lookCam = val;
}

function addHalfOrientedCircle(s : Vector3, e : Vector3, width1 : float, res : int, flip : boolean){
//var res : int = 40;
	var halfRes : int = res/2;
		
	var circle1 : Vector3[] = makeCircleVerts(s,e, width1, res, flip);
	//var circle2 : Vector3[] = makeCircleVerts(e,s, width2, res);
	
	var vl : int = ml.vertices.Length;
	
	var vs : Vector3 [] = ml.vertices;
	vs = resizeVertices(vs, halfRes+2);
	
	//Debug.Log (circle1.Length);
	
	for(var i = 0; i < halfRes+2; i++){
		vs[vl+i] = circle1[i];
	}
			
	var tl : int = ml.triangles.Length;
	
	var ts : int[] = ml.triangles;
	ts = resizeTraingles(ts, halfRes*3);
			
	var inc1: int =0;
	for(var j = 0; j < (halfRes*3); j+=3){
		//if(inc1 != halfRes-1){
		ts[tl+j] = vl; //center
		ts[tl+j+1] = vl+1+inc1;
		ts[tl+j+2] = vl+2+inc1;
		inc1++;
		//}
	}
	
	ml.vertices = vs;
	ml.triangles = ts;
	ml.RecalculateBounds();
}

function capLine(pointSet : Vector3[], startWidth : float, endWidth : float, res : int){

var s1 : Vector3 = pointSet[0];
var e1 : Vector3 = pointSet[1];

var s2 : Vector3 = pointSet[pointSet.length-2];
var e2 : Vector3 = pointSet[pointSet.length-1];

addHalfOrientedCircle(s1,e1, startWidth,res, false);
addHalfOrientedCircle(e2,s2, endWidth,res, true);

}

function addOrientedCircle(s : Vector3, e : Vector3, width1 : float, res : int ){

	//var res : int = 30;
		
	var circle1 : Vector3[] = makeCircleVerts(s,e, width1, res, false);
	//var circle2 : Vector3[] = makeCircleVerts(e,s, width2, res);
	
	var vl : int = ml.vertices.Length;
	
	var vs : Vector3 [] = ml.vertices;
	vs = resizeVertices(vs, res+1);
	
	//Debug.Log (circle1.Length);
	
	for(var i = 0; i < res+1; i++){
		vs[vl+i] = circle1[i];
	}
			
	var tl : int = ml.triangles.Length;
	
	var ts : int[] = ml.triangles;
	ts = resizeTraingles(ts, res*3);
			
	var inc1: int =0;
	for(var j = 0; j < (res*3); j+=3){
		if(inc1 != res-1){
		ts[tl+j] = vl; //center
		ts[tl+j+1] = vl+1+inc1;
		ts[tl+j+2] = vl+2+inc1;
		inc1++;
		}else{
		ts[tl+j] = vl; //center
		ts[tl+j+1] = vl+1+inc1;
		ts[tl+j+2] = vl+1;
		}

	}
	
	ml.vertices = vs;
	ml.triangles = ts;
	ml.RecalculateBounds();

}

//make a simple quad face out of 2 vectors and a width. Good stuff! returns a vector array.
function makeCircleVerts( start : Vector3,  end : Vector3,  width : float, resolution : int , flip : boolean) {
		width = width / 2;
		var q : Vector3 [] = new Vector3[resolution +1];
		
		//USING LOCAL COORDINATES
		//var localStart : Vector3 = tran.TransformPoint(start);
		//var localEnd : Vector3 = tran.TransformPoint(end);
		//USING GLOBAL COORDINATES
		var localStart : Vector3 = start;
		var localEnd : Vector3 = end;
		
		//normal
		var n: Vector3;
		if(!lookCam){
			 n  = vecDirection;
		}else{
			 //n  = Vector3.Cross(localStart, localEnd);
			 n  = Camera.main.transform.position-myObject.transform.position;
		}
		//var n : Vector3 = Vector3.Cross(localStart, localEnd);
		//perpendicular for width:
		var l : Vector3 = Vector3.Cross(n, localEnd-localStart);
		l.Normalize();
		if(flip)l*= -1;
		//
		var dir : Vector3 = localEnd-localStart;
		dir.Normalize();
		dir *= -1;
		//l is x
		//dir is y
		//circle = cos(angle) * rad, sin(angle) * rad
		
		q[0] = tran.InverseTransformPoint(localStart);
		var angle : float = 0;
		
		for(var i = 0; i < resolution; i++){
		
		
		var xCir : float = Mathf.Cos(angle) * width;
		var yCir : float = Mathf.Sin(angle) * width;
		
	
		
		var vec : Vector3 = Vector3(xCir, yCir, 0);
		
		//--------------------------
		var xComp : Vector3 = l * vec.x;
		var yComp : Vector3 = dir * vec.y;
		vec = xComp + yComp;
		//----------------------
		
		
		q[i + 1] = tran.InverseTransformPoint(localStart + vec);
		
		angle += Mathf.PI * 2/ (resolution * 1.0f);
		//q[i + 1] = tran.InverseTransformPoint(xComp + yComp);
		}
		
		
		//q[0] = tran.InverseTransformPoint(localStart + l * width);
		//q[1] = tran.InverseTransformPoint(localStart + l * -width);
		//q[2] = tran.InverseTransformPoint(localEnd + l * width);
		//q[3] = tran.InverseTransformPoint(localEnd + l * -width);

		return q;
}

function blobLineTwoSides(pointSet : Vector3[], startWidth : float, midWidth : float){
	var t1 : float = 0;
	var t2 : float = 0;
	var s : Vector3 = pointSet[0];
	
	var p0 : Vector3 = Vector3(0, startWidth,0);
	var p1 : Vector3 = Vector3(1,startWidth,0);
	var p2 : Vector3 = Vector3(1,midWidth,0);
	var p3 : Vector3 = Vector3(1,midWidth,0);
	
	var p0b : Vector3 = Vector3(1.0,midWidth,0);
	var p1b : Vector3 = Vector3(1.0,midWidth,0);
	var p2b : Vector3 = Vector3(1.0,startWidth,0);
	var p3b : Vector3 = Vector3(1.0,startWidth,0);
	
	for (var i : int = 1 ; i < pointSet.length ; i++ ){
			
		var pt : Vector3;
		if(i < pointSet.length/2){
		t1 += 2.0/pointSet.length;
		pt = GetCubicCoordinates(t1 , p0 , p1, p2, p3 );
		}else{
		t2 += 2.0/pointSet.length;
		pt = GetCubicCoordinates(t2 , p0b , p1b, p2b, p3b );
		}
			
		var wi : float = pt.y;
			if(i == 1){
				newLine(s, pointSet[i], startWidth, wi);
			}else{
				continueLine(pointSet[i], wi);
			}	
	}
}

function blobLineOneSide(pointSet : Vector3[], startWidth : float, midWidth : float){
	var t : float = 0;
	var s : Vector3 = pointSet[0];
	
	var p0 : Vector3 = Vector3(0,startWidth,0);
	var p1 : Vector3 = Vector3(0.5,startWidth,0);
	var p2 : Vector3 = Vector3(0.5,midWidth,0);
	var p3 : Vector3 = Vector3(1,midWidth,0);
	
	for (var i : int = 1 ; i < pointSet.length ; i++ ){
		t += 1.0/pointSet.length;
		var pt : Vector3 = GetCubicCoordinates(t , p0 , p1, p2, p3 );
		
		var wi : float = pt.y;
			if(i == 1){
				newLine(s, pointSet[i], startWidth, wi);
			}else{
				continueLine(pointSet[i], wi);
			}	
	}
}

//bezier equation:
function GetCubicCoordinates(t : float, p0 : Vector3 , p1 :Vector3 , p2 : Vector3, p3 : Vector3 ) : Vector3{
	return Mathf.Pow(1-t,3)*p0 + 3 *Mathf.Pow(1-t,2)*t*p1 +  
	3*(1-t)* Mathf.Pow(t,2)*p2 + Mathf.Pow(t,3)*p3;
}

function lineFromPointsVariableWidth(pointSet : Vector3[], startWidth : float, endWidth : float){

	var s : Vector3 = pointSet[0];
	for(var i = 1; i < pointSet.length; i++){
		
		var wi : float = ((endWidth - startWidth)/pointSet.length * i) + startWidth;
			if(i == 1){
				newLine(s, pointSet[i], startWidth, wi);
			}else{
				continueLine(pointSet[i], wi);
			}		
	}
}

function lineFromPoints(pointSet : Vector3[], cont : boolean){

	var s : Vector3 = pointSet[0];
	for(var i = 1; i < pointSet.length; i++){
	
		if(cont){
			if(i == 1){
				newLine(s, pointSet[i]);
			}else{
				continueLine(pointSet[i]);
			}		
		}else{
			if(i == 1){
				newLine(s, pointSet[i]);
			}else{
				continueLineNewQuads(pointSet[i]);
			}
		}
	}
}

function setMaterial(mat : Material){
lmat = mat;
}

function continueLine(e : Vector3){
//pick last point in the array
var prev : Vector3 = points[points.length-1];
 
var twoMore = MakeAdditionalVerices(prev,e,w);
incrementLine(ml, twoMore);
//add point to array:
points.Push (e);
}

function continueLine(e : Vector3, width : float ){
//pick last point in the array
var prev : Vector3 = points[points.length-1];
 
var twoMore = MakeAdditionalVerices(prev,e,width);
incrementLine(ml, twoMore);
//add point to array:
points.Push (e);
}

function continueLineNewQuads(e : Vector3){
	//pick last point in the array
	var prev : Vector3 = points[points.length-1];
 
	var qu : Vector3[] = MakeQuad(prev,e,w);
	AddLine(ml, qu, false);
	//add points to array:
	points.Push (prev);
	points.Push (e);
}

function newLine(s : Vector3, e : Vector3){
	var qu : Vector3[] = MakeQuad(s,e,w);
	AddLine(ml, qu, false);
	//add points to array:
	points.Push (s);
	points.Push (e);
}

function newLine(s : Vector3, e : Vector3, width : float){
	var qu : Vector3[] = MakeQuad(s,e,width);
	AddLine(ml, qu, false);
	//add points to array:
	points.Push (s);
	points.Push (e);
}

function newLine(s : Vector3, e : Vector3, width1 : float, width2 : float){
	var qu : Vector3[] = MakeQuad(s,e,width1, width2);
	AddLine(ml, qu, false);
	//add points to array:
	points.Push (s);
	points.Push (e);
}

function setWidth(width : float){
w = width;
}

function drawLine() {
	Graphics.DrawMesh(ml, tran.localToWorldMatrix, lmat, 0);
}
	
function incrementLine(m : Mesh,  toAdd : Vector3[]){
	var vl : int = m.vertices.Length;
	var vs : Vector3 [] = m.vertices;	
	
	vs = resizeVertices(vs, 2);
	
	vs[vl] = toAdd[0];
	vs[vl+1] = toAdd[1];
	
	var tl : int = m.triangles.Length;
	var ts : int[] = m.triangles;
	
	 ts = resizeTraingles(ts, 6);
	 ts[tl] = vl-2;
	 ts[tl+1] = vl+1-2;
	 ts[tl+2] = vl+2-2;
	 ts[tl+3] = vl+1-2;
	 ts[tl+4] = vl+3-2;
	 ts[tl+5] = vl+2-2;
	 
	 m.vertices = vs;
	 m.triangles = ts;
	 m.RecalculateBounds();
					
}
	
//line function: needing mesh to add to, vector array and boolean
function AddLine( m : Mesh,  quad : Vector3[],  tmp : boolean) {
			var vl : int = m.vertices.Length;
			
			var vs : Vector3 [] = m.vertices;
			if(!tmp || vl == 0) vs = resizeVertices(vs, 4);
			else vl -= 4;
			
			vs[vl] = quad[0];
			vs[vl+1] = quad[1];
			vs[vl+2] = quad[2];
			vs[vl+3] = quad[3];
			
			var tl : int = m.triangles.Length;
			
			var ts : int[] = m.triangles;
			if(!tmp || tl == 0) ts = resizeTraingles(ts, 6);
			else tl -= 6;
			ts[tl] = vl;
			ts[tl+1] = vl+1;
			ts[tl+2] = vl+2;
			ts[tl+3] = vl+1;
			ts[tl+4] = vl+3;
			ts[tl+5] = vl+2;
			
			m.vertices = vs;
			m.triangles = ts;
			m.RecalculateBounds();
}

function MakeAdditionalVerices( start : Vector3,  end : Vector3,  width : float) {
		width = width / 2;
		var q : Vector3 [] = new Vector3[2];
		
		//USING LOCAL COORDINATES
		//var localStart : Vector3 = tran.TransformPoint(start);
		//var localEnd : Vector3 = tran.TransformPoint(end);
		//USING GLOBAL COORDINATES
		var localStart : Vector3 = start;
		var localEnd : Vector3 = end;
		
		//normal
		var n: Vector3;
		if(!lookCam){
			 n  = vecDirection;
		}else{
			 //n  = Vector3.Cross(localStart, localEnd);
			 n  = Camera.main.transform.position-myObject.transform.position;
		}
		//var n : Vector3 = Vector3.Cross(localStart, localEnd);
		//perpendicular for width:
		var l : Vector3 = Vector3.Cross(n, localEnd-localStart);
		l.Normalize();
		
		//q[0] = transform.InverseTransformPoint(localStart + l * width);
		//q[1] = transform.InverseTransformPoint(localStart + l * -width);
		q[0] = tran.InverseTransformPoint(localEnd + l * width);
		q[1] = tran.InverseTransformPoint(localEnd + l * -width);

		return q;
}

//make a simple quad face out of 2 vectors and a width. Good stuff! returns a vector array.
function MakeQuad( start : Vector3,  end : Vector3,  width : float) {
		width = width / 2;
		var q : Vector3 [] = new Vector3[4];
		
		//USING LOCAL COORDINATES
		//var localStart : Vector3 = tran.TransformPoint(start);
		//var localEnd : Vector3 = tran.TransformPoint(end);
		//USING GLOBAL COORDINATES
		var localStart : Vector3 = start;
		var localEnd : Vector3 = end;
		
		//normal *************************************************************************************************************************
		var n: Vector3;
		if(!lookCam){
			 n  = vecDirection;
		}else{
			 //n  = Vector3.Cross(localStart, localEnd);
			 n  = Camera.main.transform.position-myObject.transform.position;
		}
		//perpendicular for width:
		var l : Vector3 = Vector3.Cross(n, localEnd-localStart);
		l.Normalize();
		
		q[0] = tran.InverseTransformPoint(localStart + l * width);
		q[1] = tran.InverseTransformPoint(localStart + l * -width);
		q[2] = tran.InverseTransformPoint(localEnd + l * width);
		q[3] = tran.InverseTransformPoint(localEnd + l * -width);

		return q;
}

function MakeQuad( start : Vector3,  end : Vector3,  width1 : float,  width2 : float) {
		width1 = width1 / 2;
		width2 = width2 / 2;
		var q : Vector3 [] = new Vector3[4];
		
		//USING LOCAL COORDINATES
		//var localStart : Vector3 = tran.TransformPoint(start);
		//var localEnd : Vector3 = tran.TransformPoint(end);
		//USING GLOBAL COORDINATES
		var localStart : Vector3 = start;
		var localEnd : Vector3 = end;
		
		//normal
		var n: Vector3;
		if(!lookCam){
			 n  = vecDirection;
		}else{
			 //n  = Vector3.Cross(localStart, localEnd);
			 n  = Camera.main.transform.position-myObject.transform.position;
		}
		//perpendicular for width:
		var l : Vector3 = Vector3.Cross(n, localEnd-localStart);
		l.Normalize();
		
		q[0] = tran.InverseTransformPoint(localStart + l * width1);
		q[1] = tran.InverseTransformPoint(localStart + l * -width1);
		q[2] = tran.InverseTransformPoint(localEnd + l * width2);
		q[3] = tran.InverseTransformPoint(localEnd + l * -width2);

		return q;
}
	
function resizeVertices (ovs : Vector3[],  ns : int) {
		var nvs : Vector3[] = new Vector3[ovs.Length + ns];
		for(var i = 0; i < ovs.Length; i++) nvs[i] = ovs[i];
		return nvs;
}
	
function resizeTraingles(ovs : int[],  ns : int) {
		var nvs : int[] = new int[ovs.Length + ns];
		for(var i = 0; i < ovs.Length; i++) nvs[i] = ovs[i];
		return nvs;
}


}







