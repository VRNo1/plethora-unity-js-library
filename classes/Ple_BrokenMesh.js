#pragma strict

class Ple_BrokenMesh{

	//MESH:
	private var ml : Mesh;
	var lmat : Material;
	private var tran : Transform;


	function Ple_BrokenMesh(_tran : Transform){
		tran = _tran;
		ml = new Mesh();
	}
	
	
	function setMaterial(mat : Material){
		lmat = mat;
	}

	function clearMesh(){
		ml.Clear();
	}

	function drawMesh() {
		Graphics.DrawMesh(ml, tran.localToWorldMatrix, lmat, 0);
	}
	
	function addQuad(quad : Vector3[],  tmp : boolean) {
			
			//VERTICES
			var vl : int = ml.vertices.Length;
			var vs : Vector3 [] = ml.vertices;
			if(!tmp || vl == 0) vs = resizeVertices(vs, 4);
			else vl -= 4;
			
			vs[vl] = quad[0];
			vs[vl+1] = quad[1];
			vs[vl+2] = quad[2];
			vs[vl+3] = quad[3];
					
			//TRIANGLES		
			var tl : int = ml.triangles.Length;
			var ts : int[] = ml.triangles;
			if(!tmp || tl == 0) ts = resizeTraingles(ts, 6);
			else tl -= 6;
			ts[tl] = vl;
			ts[tl+1] = vl+1;
			ts[tl+2] = vl+2;
			ts[tl+3] = vl+1;
			ts[tl+4] = vl+3;
			ts[tl+5] = vl+2;
			
			//UVS
			var ul : int = ml.uv.Length;
			var us : Vector2 [] = ml.uv;
			if(!tmp || ul == 0) us = resizeUVs(us, 4);
			else ul -= 4;
			
			us[vl] = Vector2(0,1);
			us[vl+1] = Vector2(0,1);
			us[vl+2] = Vector2(0,1);
			us[vl+3] = Vector2(0,1);
			
			
			ml.vertices = vs;
			ml.triangles = ts;
			ml.uv = us;
			//ml.RecalculateBounds();
			
	}
	
	function recalculateNormals(){
		ml.RecalculateNormals();
	}
	
	function makeArray( p1 : Vector3,p2: Vector3,p3: Vector3,p4: Vector3) {
		var q : Vector3 [] = new Vector3[4];
		
		q[0] = tran.InverseTransformPoint(p1);
		q[1] = tran.InverseTransformPoint(p2);
		q[2] = tran.InverseTransformPoint(p3);
		q[3] = tran.InverseTransformPoint(p4);

		return q;
	}

	function resizeVertices (ovs : Vector3[],  ns : int) {
		var nvs : Vector3[] = new Vector3[ovs.Length + ns];
		for(var i = 0; i < ovs.Length; i++) nvs[i] = ovs[i];
		return nvs;
	}
	
	function resizeUVs (ovs : Vector2[],  ns : int) {
		var nvs : Vector2[] = new Vector2[ovs.Length + ns];
		for(var i = 0; i < ovs.Length; i++) nvs[i] = ovs[i];
		return nvs;
	}
	
	function resizeTraingles(ovs : int[],  ns : int) {
		var nvs : int[] = new int[ovs.Length + ns];
		for(var i = 0; i < ovs.Length; i++) nvs[i] = ovs[i];
		return nvs;
	}

}