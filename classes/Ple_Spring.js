#pragma strict

class Ple_Spring{

	var a : Ple_Particle;
	var b : Ple_Particle;
	
	var restLength : float;
	
	var stiffness : float = 0.5; //k
	
	//var forceApplied : float = 0.0;

	function Ple_Spring(_a : Ple_Particle, _b : Ple_Particle){
	
		this.a = _a;
		this.b = _b;
		this.restLength = Vector3.Distance(a.loc,b.loc);
	}
	
	//SODA METHOD
	function calculateForces(){	
		var dist : float = Vector3.Distance(a.loc,b.loc);
		if(dist > 0){
		
		  var f : float = (dist-restLength)* stiffness;
	      var fH : float  = (f/dist)*(a.loc.x-b.loc.x);
	      var fV : float = (f/dist)*(a.loc.y-b.loc.y);
	      a.vel.x -= fH;
	      a.vel.y -= fV;
	      b.vel.x += fH;
	      b.vel.y += fV;
		
		}	
	}
	
	//NOC METHOD
	function calculateForces2(){
	
	var force : Vector3 = b.loc - a.loc;
	var dist : float = Vector3.Distance(a.loc,b.loc);
	if(dist > 0){
	
	  var stretch : float = dist - restLength;
		
 	  force.Normalize();
      force *= (-1.0 * stiffness * stretch);
      
      b.addForce2(force);
	  //forceApplied = force.magnitude;
	}
	
	
	}
	
	function constrainLength(minlen: float, maxlen : float){
	
		var dir : Vector3 = b.loc - a.loc;
		    
	    var d : float = Vector3.Distance(a.loc,b.loc);
	    // Is it too short?
	    if (d < minlen) {
	      dir.Normalize();
	      dir *= (minlen);
	      // Reset location and stop from moving (not realistic physics)
	      b.loc = a.loc + dir;
	      b.vel *= 0;
	      // Is it too long?
	    } 
	    else if (d > maxlen) {
	      dir.Normalize();
	      dir *= (maxlen);
	      // Reset location and stop from moving (not realistic physics)
	      b.loc = a.loc + dir;
	      b.vel *= 0;
	    }
    
    
	}
	
	function setRestLength(v : float){
	restLength = v;
	}
	
	function setStiffness(v : float){
	stiffness = v;
	}


}