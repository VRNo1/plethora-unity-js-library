#pragma strict


class Ple_Bezier{

private var pt1 : Vector3;
private var pt2 : Vector3;
private var pt3 : Vector3;
private var pt4 : Vector3;

private var points : List.<Vector3> = new List.<Vector3>();

//var numPts: int = 6;
var debugOn : boolean = false;

function Ple_Bezier(_pt1 : Vector3, _pt2 : Vector3,_pt3 : Vector3,_pt4 : Vector3){

pt1 = _pt1;
pt2 = _pt2;
pt3 = _pt3;
pt4 = _pt4;

}

function Ple_Bezier(_pt1 : GameObject, _pt2 : GameObject,_pt3 : GameObject,_pt4 : GameObject){

pt1 = _pt1.transform.position;
pt2 = _pt2.transform.position;
pt3 = _pt3.transform.position;
pt4 = _pt4.transform.position;

}


function setDebug(val : boolean){
	debugOn = val;
}

function updateControlPoints(_pt1 : GameObject, _pt2 : GameObject,_pt3 : GameObject,_pt4 : GameObject){
pt1 = _pt1.transform.position;
pt2 = _pt2.transform.position;
pt3 = _pt3.transform.position;
pt4 = _pt4.transform.position;
}

function updateControlPoints(_pt1 : Vector3, _pt2 : Vector3,_pt3 : Vector3,_pt4 : Vector3){
pt1 = _pt1;
pt2 = _pt2;
pt3 = _pt3;
pt4 = _pt4;
}

//create points and return generic list:
function createBezierPoints(numPts : int){
	var t : float = 0;
	points.Clear();
	for (var i : int = 0 ; i < numPts ; i++ ){
			 
			var pt : Vector3 = GetCubicCoordinates(t , pt1 , pt2, pt3, pt4 );
			points.Add(pt);
			t  += 1.0/(numPts-1) ;
	}
	
	//debug graphics:
	if (debugOn){	
	for (var j : int = 1 ; j < points.Count ; j++ ){	
		Debug.DrawLine(points[j], points[j-1],  Color.blue);
	}
		Debug.DrawLine(pt1, pt2,  Color.red);
		Debug.DrawLine(pt3, pt4,  Color.red);
	}
	
	return points;
}


function GetCubicCoordinates(t : float, p0 : Vector3 , p1 :Vector3 , p2 : Vector3, p3 : Vector3 ) : Vector3{
	return Mathf.Pow(1-t,3)*p0 + 3 *Mathf.Pow(1-t,2)*t*p1 +  
	3*(1-t)* Mathf.Pow(t,2)*p2 + Mathf.Pow(t,3)*p3;
}


}