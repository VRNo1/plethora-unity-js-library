#pragma strict



class Toxi_VerletSpring{

//private var EPS = 1.0 - 6f;
private var EPS = 0.000001;


var a : Toxi_VerletParticle;
var b : Toxi_VerletParticle;

var restLength: float;
var strength : float;

var isALocked : boolean;
var isBLocked : boolean;

function Toxi_VerletSpring(_a: Toxi_VerletParticle, _b: Toxi_VerletParticle, _restLength : float, _strength : float){

a = _a;
b = _b;

restLength = _restLength;
strength = _strength;

}

//for making the restlength the distance between particles
function Toxi_VerletSpring(_a: Toxi_VerletParticle, _b: Toxi_VerletParticle, _strength : float){

a = _a;
b = _b;

var dist = Vector3.Distance(a.loc,b.loc);

restLength = dist;
strength = _strength;

}

//update
function update(){

	var delta : Vector3 = b.loc - a.loc;
	
	var dist = delta.magnitude +  EPS; //see if could work with sqr magnitude
	
	var  normDistStrength : float = (dist - restLength) / (dist * (a.invWeight + b.invWeight)) * strength;
					
	if(!a.isLocked && !isALocked){
		a.loc += (delta * normDistStrength * a.invWeight);
		//a.addSelf(delta.scale(normDistStrength * a.invWeight));
		//APPLY CONSTRAINS
	}
	if(!b.isLocked && !isBLocked){
		b.loc += (delta * -normDistStrength * b.invWeight);
	}				
}

function updateRigid(){

	var delta : Vector3 = b.loc - a.loc;
	var dist = delta.magnitude ; //see if could work with sqr magnitude
	
	var  normDistStrength : float = (dist - restLength) / (dist);
	var  diff : float = (dist - restLength);
	var vec : Vector3 = delta.normalized;
	vec *=  diff/2;
					
	if(!a.isLocked && !isALocked){
		a.loc += vec;
	}
	if(!b.isLocked && !isBLocked){
		b.loc += -vec;
	}				
}


//getters:
function getRestLength(){
	return restLength;
}

function getStrength(){
	return strength;
}

//setters:
function setRestLength(v : float){
	 restLength = v;
}
function setStrength(v : float){
	 strength = v;
}


}