#pragma strict
//-------------------------------------------------------------------
//UNITY CUSTOM CLASSES
//BY JOSE SANCHEZ
//DEVELOPED FOR: PLETHORA-PROJECT.COM
//-------------------------------------------------------------------

//intro tutorial:
//class declaration:
class NodeAttributes{

var id : int;
var name : String;

}

//-------------------------------------------------------------------
//NOT WORKING:
class Ple_Agent{

var loc : Vector3;
var id : int;

function Ple_Agent(_loc : Vector3, _id : int){

loc = _loc;
id = _id;

var ps : ParticleSystem;
//this would need a game object:
//ps = GameObject.AddComponent ("ParticleSystem");

}


}

//-------------------------------------------------------------------
//NOT WORKING:
//particle grid 
class Ple_Voxel{

function Ple_Voxel(gObj : GameObject){

}

}








//NOT WORKING:
//-------------------------------------------------------------------
class Ple_SpringTest01 {
 
  var anchor : GameObject;
 

  var restLen : float ; //desired length = rest length
  var  damp : float = 0.01; //damp????
 
 function Ple_SpringTest01( a : GameObject,  _restLen : int, _damp : float) {
    anchor = a;
    restLen = _restLen;
    damp = _damp;
  }
 

  function connect( b : Transform) {
 
    var force : Vector3 =  anchor.transform.position - b.position ;
    //var dist : float  = force.magnitude();
    var dist = (b.position-anchor.transform.position).sqrMagnitude;
    var stretch : float = dist - restLen;
 
    force.Normalize();
    force *= (-1 * damp * stretch);
 	
 	
    anchor.transform.position += force;
  }
 
  //void display() {
   // fill(100);
   // rectMode(CENTER);
  //  rect(anchor.x,anchor.y,10,10);
  //}
 
  //void displayLine(Bob b) {
  //  stroke(255);
  //  line(b.location.x,b.location.y,anchor.x,anchor.y);
  //}
 
}



//NOT WORKING:
//-------------------------------------------------------------------
class Ple_VectorSpring {
 
  var anchor : Vector3;
  var restLen : float ; //desired length = rest length
  var  damp : float = 0.01; //damp????
 
 function Ple_VectorSpring( a : Vector3,  _restLen : int, _damp : float) {
    anchor = a;
    restLen = _restLen;
    damp = _damp;
  }
 

  function connect( b : Vector3) {
 
    var force : Vector3 =  anchor - b ;
    //var dist : float  = force.magnitude();
    var dist = (b-anchor).sqrMagnitude;
    var stretch : float = dist - restLen;
 
    force.Normalize();
    force *= (-1 * damp * stretch);
 		
    //anchor.transform.position += force;
    return(force);
  }
}

//NOT WORKING:
//-------------------------------------------------------------------
class Ple_VectorSpring2 {
 
 var k : float = 0.2; //spring constant
 var tempPosX : float;
 var tempPosY : float;
 
  var resPosX : float;
 var resPosY : float;
 
 var  damp : float = 0.01; 
 
  var anchor : Vector3;
  var restLen : float ; //desired length = rest length
  
 
 function Ple_VectorSpring2( a : Vector3,  _restLen : int, _damp : float) {
    anchor = a;
    restLen = _restLen;
    damp = _damp;
  }
 

  function connect( b : Vector3) {
 
    // force = -k * (tempypos - rest_posy);  // f=-ky 
    //accel = force / mass;                 // Set the acceleration, f=ma == a=f/m 
    //vely = damp * (vely + accel);         // Set the velocity 
    //tempypos = tempypos + vely;           // Updated position 
 
    //var forceX : Vector3 = -k *( tempPosX - resPosX)  ;
	
 		
    //anchor.transform.position += force;
    //return(force);
  }
}

//NOT WORKING:
//-------------------------------------------------------------------
class Ple_VectorRepel {
 
  var origin : Vector3;
  var  scale : float = 1;
 
 function Ple_VectorRepel( _origin : Vector3,  _scale : float) {
    origin = _origin;
    scale = _scale;
  }
 

  function repel( target : Vector3) {
    var force : Vector3 =  target - origin ;
    force.Normalize();
    force *= scale;
    return(force);
  }
}
//-------------------------------------------------------------------



function reMap(val : float, from1 : float, to1 : float, from2 : float, to2 : float){
return (val - from1) / (to1 - from1) * (to2 - from2) + from2;
}


































































