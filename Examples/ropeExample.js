#pragma strict

//springs and physics:-------------------------------------------------------
private var partA : Toxi_VerletParticle;
private var partB : Toxi_VerletParticle;
private var sp : Toxi_VerletSpring ;
private var vPhy : Toxi_VerletPhyisics3D = new Toxi_VerletPhyisics3D();

var restLength : float = 2;
var strength : float = 0.1;
var force : float = -0.01;
var drag : float = 0.01;
//----------------------------------------------------------------------------------------------------------------------------------
//line mesh
var myLine : Ple_LineMesh;
var mat : Material;
var width : float = 0.1;
//----------------------------------------------------------------------------------------------------------------------------------

function Start () {

//----------------------------------------------------------------------------------------------------------------------------------
//CREATION OF A CHAIN SEQUENCE
for(var i = 0; i < 10; i++){
	var p : Toxi_VerletParticle  = new Toxi_VerletParticle(gameObject.transform.position.x+ (i*-0.1),gameObject.transform.position.y + (i*-0.1),gameObject.transform.position.z);
	if(i == 0){
		p.setLock(true);
	}
	p.addForce(Vector3(0,-0.1,0));
	
	vPhy.addParticle(p);
	if(i > 0){
		var s : Toxi_VerletSpring = new Toxi_VerletSpring(p,vPhy.particleList[i-1], 2, 0.1);
		vPhy.addSpring(s);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
//MESH LINE
	myLine = new Ple_LineMesh(gameObject);
	myLine.setWidth(0.1);
	
	myLine.setMaterial(mat);
}

function Update () {

vPhy.update();

vPhy.setSpringRestLength(restLength);
vPhy.setSpringStrength(strength);
vPhy.applyForce(Vector3(0,force,0));

vPhy.setDrag(drag);
//----------------------------------------------------------------------------------------------------------------------------------
//reset mesh:
myLine.clearMesh();

var count = 0;
for (var spy : Toxi_VerletSpring in vPhy.springsList){
	Debug.DrawLine (spy.a.loc, spy.b.loc, Color.red);
	Debug.DrawLine (spy.a.loc, Vector3(spy.a.loc.x + 1, spy.a.loc.y, spy.a.loc.z), Color.blue);
	
	//create mesh:
	if(count == 0){
	myLine.newLine(spy.a.loc, spy.b.loc);
	}else{
	myLine.continueLine(spy.b.loc);
	}
	count++;
}
//----------------------------------------------------------------------------------------------------------------------------------
//DRAW MESH:
//myLine.drawLine();

var p1 = vPhy.getParticle(0);
p1.setLoc(gameObject.transform.position);


}