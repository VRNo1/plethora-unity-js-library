#pragma strict

private var partA : Toxi_VerletParticle;
private var partB : Toxi_VerletParticle;
private var sp : Toxi_VerletSpring ;

//var restLength : float = 2;
//var strength : float = 0.1;
//var force : float = -0.01;
//var drag : float = 0.01;

var myLine : Ple_LineMesh;
var mat : Material;
var width : float = 0.1;

var meshRes : int = 40;
var points : Vector3[];

function Start () {

var relative : Vector3 = transform.InverseTransformDirection(gameObject.transform.localPosition );

//initialize particles:
partA = new Toxi_VerletParticle(relative.x,relative.y,relative.z);
partB = new Toxi_VerletParticle(relative.x,relative.y,relative.z);

partA.setLock(true);
partB.addForce(Vector3(0,-0.1,0));

//initialize spring
sp = new Toxi_VerletSpring(partA,partB, 1, 0.1);

//initialize mesh:
myLine = new Ple_LineMesh(gameObject);
myLine.setWidth(1);
myLine.setMaterial(mat);

//look at camera:
//myLine.setCamLook(true);

points = new Vector3[meshRes];
}

function Update () {
	
	//update particles
	partA.update();
	partB.update();
	//update springs
	sp.update();
	
	//var relative : Vector3 = transform.InverseTransformDirection(gameObject.transform.localPosition );
	//var dif : Vector3 = gameObject.transform
	partA.loc = gameObject.transform.position;//relative;//gameObject.transform.position;
	
	
	//point array for blob mesh
	var didi : float = Vector3.Distance(partA.loc ,partB.loc);
		
	//DEFINE A SERIES OF POINTS BETWEEN THEM
	//updatePoint array
	for(var k = 0; k < meshRes; k++){
		var v : Vector3 =  partB.loc - partA.loc;	
		v.Normalize();
		v *= didi/meshRes * k;	
		var pt : Vector3 = partA.loc + v;
		points[k] = pt; 
	}
	
	var wi : float = reMap(didi, 0.5 , 1.5, 1, 0.5) ;
	//drawLine
	myLine.clearMesh();
	//myLine.newLine(partA.loc,partB.loc);
	
	myLine.blobLineTwoSides(points, 1,wi);
	myLine.capLine(points, 1, 1, 12);
	
	myLine.drawLine();
	
	Debug.DrawLine (partA.loc, partB.loc, Color.red);
}

function reMap(val : float, from1 : float, to1 : float, from2 : float, to2 : float){
	return (val - from1) / (to1 - from1) * (to2 - from2) + from2;
}





















