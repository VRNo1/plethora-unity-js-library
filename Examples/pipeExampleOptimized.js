#pragma strict

var pipe : Ple_MeshPipe;
var bez : Ple_Bezier;

var pt1 : GameObject;
var pt2 : GameObject;
var pt3 : GameObject;
var pt4 : GameObject;

private var prevPt1 : Vector3;
private var prevPt2 : Vector3;
private var prevPt3 : Vector3;
private var prevPt4 : Vector3;

var mat : Material;

var BezSegments : int = 10;

private var pts : List.<Vector3>;

function Start () {

prevPt1 = pt1.transform.position;
prevPt2 = pt2.transform.position;
prevPt3 = pt3.transform.position;
prevPt4 = pt4.transform.position;


//BEZIER: ------------------------------------------------------------------------------------------------
//create bezier out of 4 vectors
bez = new Ple_Bezier(pt1,pt2,pt3,pt4);
bez.updateControlPoints(pt1,pt2,pt3,pt4);
pts = bez.createBezierPoints(BezSegments);
//--------------------------------------------------------------------------------------------------------

//PIPE MESH: ---------------------------------------------------------------------------------------------
pipe = new Ple_MeshPipe(gameObject.transform);
pipe.setMaterial(mat);
pipe.createVertices(pts);
//--------------------------------------------------------------------------------------------------------

}

function Update () {
	
	//Check if there has been any changes to the points:
	if(prevPt1 != pt1.transform.position || prevPt2 != pt2.transform.position || prevPt3 != pt3.transform.position || prevPt4 != pt4.transform.position){
		
		print ("updated");
		prevPt1 = pt1.transform.position;
		prevPt2 = pt2.transform.position;
		prevPt3 = pt3.transform.position;
		prevPt4 = pt4.transform.position;
		
		//BEZIER: ------------------------------------------------------------------------------------------------
		bez.updateControlPoints(pt1,pt2,pt3,pt4);
		pts = bez.createBezierPoints(BezSegments);
		//PIPE MESH: ---------------------------------------------------------------------------------------------
		pipe.createVertices(pts);
		
	}else{
		print ("no update");
	}
	
	//PIPE MESH: ---------------------------------------------------------------------------------------------
	pipe.drawMesh();

}