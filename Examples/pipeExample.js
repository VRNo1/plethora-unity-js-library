#pragma strict

var pipe : Ple_MeshPipe;
var bez : Ple_Bezier;

var pt1 : GameObject;
var pt2 : GameObject;
var pt3 : GameObject;
var pt4 : GameObject;

var mat : Material;

var BezSegments : int = 10;

function Start () {


//BEZIER: ------------------------------------------------------------------------------------------------
//create bezier out of 4 vectors
bez = new Ple_Bezier(pt1,pt2,pt3,pt4);
//set debug mode to true to see graphics on the viewport
//bez.setDebug(true);
//--------------------------------------------------------------------------------------------------------

//PIPE MESH: ---------------------------------------------------------------------------------------------
pipe = new Ple_MeshPipe(gameObject.transform);
pipe.setMaterial(mat);
//--------------------------------------------------------------------------------------------------------

}

function Update () {

//BEZIER: ------------------------------------------------------------------------------------------------
//update controlPoints:
bez.updateControlPoints(pt1,pt2,pt3,pt4);
//recalculate points every frame:
var pts : List.<Vector3> = bez.createBezierPoints(BezSegments);
//--------------------------------------------------------------------------------------------------------

//PIPE MESH: ---------------------------------------------------------------------------------------------
//pipe.setDebug(true);
pipe.createVertices(pts);
pipe.drawMesh();
//--------------------------------------------------------------------------------------------------------

}