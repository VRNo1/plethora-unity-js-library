#pragma strict

//Broken mesh is very slow...
//only usefull for few number of polygons...

var ml : Ple_BrokenMesh;
var mat : Material;

var COLS : int;
var ROWS : int;

function Start () {

//start the mesh:
ml = new Ple_BrokenMesh(gameObject.transform);
ml.setMaterial(mat);

///*
for(var i = 0; i < COLS; i++){
	for(var j = 0; j < ROWS; j++){

		
		var v : Vector3 [] = new Vector3[4];
		
		v[0] = Vector3 (i,0,j);
		v[2] = Vector3 (i+1,0,j);
		v[3] = Vector3 (i+1,0,j+1);
		v[1] = Vector3 (i,0,j+1);
		
		//add quads to the mesh:
		ml.addQuad( v, false );

	}
}
ml.recalculateNormals();
//*/

}

function Update () {

/*
ml.clearMesh();

for(var i = 0; i < COLS; i++){
	for(var j = 0; j < ROWS; j++){
	
		var v : Vector3 [] = new Vector3[4];
		
		v[0] = Vector3 (i,0,j);
		v[2] = Vector3 (i+1,0,j);
		v[3] = Vector3 (i+1,0,j+1);
		v[1] = Vector3 (i,0,j+1);
		
		//add quads to the mesh:
		ml.addQuad( v, false );

	}
}

ml.recalculateNormals();
*/

//draw the mesh:
ml.drawMesh();

}