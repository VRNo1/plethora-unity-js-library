#pragma strict

//pendulum class:
private var myPen : Ple_Pendulum ;
//line class:
private var myLine : Ple_LineMesh;
var mat : Material;
var width : float = 0.1;

function Start () {

myPen = new Ple_Pendulum(gameObject.transform.position, 3, Mathf.PI/9);

myLine = new Ple_LineMesh(gameObject);
myLine.setWidth(0.1);
	
myLine.setMaterial(mat);

}

function Update () {
	
	myPen.setOrigin(gameObject.transform.position);

	myPen.setDamping(0.9999);
	myPen.setGravity(0.01);
	//calculate
	myPen.calculate();

	//get results:
	var o : Vector3 = myPen.getOrigin();
	var loc : Vector3 = myPen.getLocation();
	
	//clear mesh every frame
	myLine.clearMesh();
	//create a line
	myLine.newLine(o,loc);
	//circle at end of circle: (pt1, pt2, radius, resolution)
	myLine.addOrientedCircle(loc,o, 1 ,8);
	//draw all meshes
	myLine.drawLine();

}